runBMLGrid <-
function( g, NumSteps ){
	row = nrow( g )
	col = ncol( g )
	newg = matrix( 0, row, col )
	redCarloc = which( g==1, arr.ind = T)
	blueCarloc = which( g==2, arr.ind = T)
	movecar = rep( 0 , NumSteps )
	
	res = .c("crumBMLGrid", as.integer( g ), as.integer( newg ), as.integer( row ), as.integer( col ), as.integer( NumStpes ), 
as.integer( redCarloc ), as.integer( nrow( redCarloc )), as.integer(blueCarloc), as.integer( nrow(blueCarloc)) ,
as.integer( movecar))
	
	velocity = res$movecar/( nrow( redCarloc ) + nrow( blueCarloc))
	return( list( res$g, velocity  )
}