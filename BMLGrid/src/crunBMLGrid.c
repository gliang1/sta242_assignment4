#include <stdio.h>
 #include <string.h>
 #include "cmoveCar.h"  
 
void
crunBMLGrid( int *g, int *newg, int *row, int*col, int *NumSteps,
            int *redCarLoc, int *NumredCar, int *blueCarLoc, int *NumblueCar,
            int *movecar)
{
    int NSteps = NumSteps[ 0 ];
    
    //main loop for NSteps
    int i;
    int Cartag[1] = {0};
    for( i=0; i< NSteps; i++ ){
        int NumMove[1] = {0};
        //move red car
        Cartag[0] = 1;
        cMoveCar( g, newg, row, col, redCarLoc, NumredCar, NumMove, Cartag);
        
         memcpy( g, newg, sizeof( int )*row[0]*col[0] );
        
        
        //move blue car
        Cartag[0] = 2;
        cMoveCar( g, newg, row, col, blueCarLoc, NumblueCar, NumMove, Cartag);
        
         memcpy( g, newg, sizeof( int )*row[0]*col[0] );
        
        
        movecar[ i ] = NumMove[0];
    }
    
   
    
}
