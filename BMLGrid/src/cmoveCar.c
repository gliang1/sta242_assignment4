 #include <stdio.h>
 #include <string.h>
void
cMoveCar(int *g, int *newg, int *row, int*col, int *CarLoc,
         int *NumCar, int *NumMove, int *Cartag)
{
    int tagNum = Cartag[0];
    int rowLoc, colLoc, NextrowLoc, NextcolLoc;
    int moveCar = 0;
    
    
    memcpy(newg, g, sizeof(int)*row[0] * col[0]);
    
    int k = 0;
    for (k = 0; k < NumCar[0]; k++){
        //Now position
        rowLoc = CarLoc[k];
        colLoc = CarLoc[k + NumCar[0]];
        
        
        //Next position
        if (tagNum == 1){
            NextrowLoc = rowLoc + 1;
            NextcolLoc = colLoc;
            if (NextrowLoc > row[0]) NextrowLoc = 1;
            

        }
        else{
            NextrowLoc = rowLoc;
            NextcolLoc = colLoc - 1;
            if (NextcolLoc == 0) NextcolLoc = col[0];

        }
		
        //move car
        if (g[NextrowLoc - 1 + row[0] * (NextcolLoc - 1 )] == 0){
            newg[NextrowLoc -1 + row[0] * ( NextcolLoc - 1) ] = tagNum;
            newg[rowLoc - 1 + row[0]*(colLoc - 1) ] = 0;
            CarLoc[k] = NextrowLoc;
            CarLoc[k + NumCar[0]] = NextcolLoc;
            moveCar++;
        }
        
        
    }
    NumMove[0] = NumMove[0] + moveCar;
    
}