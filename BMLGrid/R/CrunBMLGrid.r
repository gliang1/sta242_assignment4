CrunBMLGrid <-
function( g, NumSteps ){
    row = nrow( g )
    col = ncol( g )
    newg = matrix( 0, row, col )
    redCarloc = which( g==1, arr.ind = T)
    blueCarloc = which( g==2, arr.ind = T)
    movecar = rep( 0 , NumSteps )
    
    res = .C("crunBMLGrid", as.integer( g ), as.integer( newg ), as.integer( row ), as.integer( col ), as.integer( NumSteps ),
    as.integer( redCarloc ), as.integer( nrow( redCarloc )), as.integer(blueCarloc), as.integer( nrow(blueCarloc)) ,
    as.integer( movecar))
    g = matrix( res[[1]], row, col)
    class( g ) <- append( "BMLGrid",class( g ))
    velocity = res[[10]]/( nrow( redCarloc ) + nrow( blueCarloc))
    return(  list( g, velocity  ))
}