testVelocity <-
function( r, c, rho, NumbSteps ){
    #empty matrix for ratio
  ratioM = matrix( rep( 0, length( rho)*NumbSteps ), nrow = length( rho ))
  for( i in 1:length( rho )){
    ratioM[ i, ]= MainBMLGrid( r, c, rho[i], NumbSteps)[[2]]
  }
  return( ratioM )
}
