\name{runBMLGrid}
\alias{runBMLGrid}
\title{
Moveing Cars Many Times
}
\description{
Move the cars by find the cars in the Grid, and determine whether the cell to which it would move is currently vacant, and update all the cars' location which could move. 

Repeat this step N times.
}
\usage{
runBMLGrid(g, NumSteps)
}
\arguments{
  \item{g}{
A matrix, which is usually generate by CreateBMLGrid.
}
  \item{NumSteps}{
The number of steps
}
}

\value{
\item{g }{The Matrix updated after NumSteps}
\item{ratio }{ A vector contains each steps' car velocity }
}

\examples{
Grid0 = CreateBMLGrid( 100, 100, 0.2)
res = runBMLGrid( Grid0, 1000)
Grid1000 = res[[ 1 ]]
ratio = res[[ 2 ]]

## The function is currently defined as
function (g, NumSteps) 
{
    ratio = rep(0, NumSteps)
    for (i in 1:NumSteps) {
        temp = MoveCar_3(g)
        g = temp[[1]]
        ratio[i] = temp[[2]]
    }
    class(g) <- append("BMLGrid", class(g))
    return(list(g, ratio = ratio))
  }
}

