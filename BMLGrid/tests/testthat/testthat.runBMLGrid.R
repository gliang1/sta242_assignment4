library(BMLGrid)
context("test of runBMLGrid function")

test_that("The  Grid should be equal", {
  g = matrix( c( 0, 0, 2, 1, 1, 2,1,0,0,0, 2,0), 4, 3)
  g0 = matrix( c( 1, 2,2,0,1,0,2,1,0,0,0,0),4, 3 )
  class( g0 ) = c( "BMLGrid", class( g0 ))
  expect_equal( runBMLGrid( g, 1)[[ 1 ]], g0)  
})