// #include "  .h"   include the needed header file
void
crunBMLGrid( int *g, int *newg, int *row, int*col, int*NumSteps, 
int *redCarLoc, int *NumredCar, int *blueCarLoc, int *NumblueCar,
double *Velocity )
{
	  //All the new variables I needed and their type
	  //NRow represents the number of row of Matrix g
	  //NCol represents the number of column of Matrix g
	  //NSteps represents the number of steps
	int NRow = row[ 0 ];
	int NCol = col[ 0 ];
	int NSteps = NumSteps[ 0 ];
	
	  //main loop for NSteps
	for( i=0; i<= NSteps, i++ ){
	  //move red car
	  Cartag = 1;
	  cMoveCar( g, newg, row, col, redCarLoc, NumredCar, NumMove, Cartag)
	  //move blue car
	  cMoveCar( g, newg, row, col, blueCarLoc, NumblueCar, NumMove, Cartag)  	
	}
	
	memcpy( g, newg, sizeof( int )*row[0]*col[0] );
	
}


void
cMoveCar( int *g, int *newg, int *row, int*col, int *CarLoc, int *NumCar, int *NumMove, int *Cartag )
{
	int tagNum = Cartag[0];
	int rowLoc, colLoc, NextrowLoc, NextcolLoc
	int moveCar = 0
	
	memcpy( newg, g, sizeof( int )*row[0]*col[0] );
	
	int k = 0;
	for( k = 0; k < NumCar[0], k++){
		  //Now position
		rowLoc = CarLoc[ k ];
		colLoc = CarLoc[ k + NumCar[ 0 ] ];
		
		  //Next position
		if( tagNum == 1){
			NextrowLoc = rowLoc +1;
			NextcolLoc = colLoc;
			if( NextrowLoc > row[0]) NextrowLoc = 1;
		}else{
			NextrowLoc = rowLoc;
			NextcolLoc = colLoc-1;
			if( NextrowLoc == 0 ) NextcolLoc = col[ 0 ];
		}
		  //move car
		if( g[ NextrowLoc + row[0]*NextcolLoc -1 ] != 0 ){
			newg[ NextrowLoc + row[0]*NextcolLoc -1 ] = g[ rowLoc + row[0]*colLoc -1];
			CarLoc[ k ] = NextrowLoc;
			CarLoc[ k + NumCar[ 0 ]] = NextcolLoc;
			moveCar++;
		}
		
	}
	NumMove[ 0 ] = NumMove[0]+moveCar;
	
}